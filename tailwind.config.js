module.exports = {
  purge: ["./src/**/*.{js,jsx,ts,tsx}", "./public/index.html"],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      screens: {
        xs: "200px",
        phone: "428px",
        laptop: "1080px",
        desktop: "1280px",
      },
      zIndex: {
        "-10": "-10",
        "-20": "-20",
      },
      spacing: {
        "v-50": "50vh",
        "v-60": "60vh",
        "v-70": "70vh",
        "v-80": "80vh",
      },
      colors: {
        gold: {
          50: "#fdfaf4",
          100: "#fbf4e9",
          200: "#f6e4c8",
          300: "#FEC502",
          400: "#e5b364",
          500: "#da9221",
          600: "#c4831e",
          700: "#a46e19",
          800: "#835814",
          900: "#645555",
        },
        blueberry: {
          50: "#f5f7fa",
          100: "#ebeef4",
          200: "#cdd5e4",
          300: "#aebbd4",
          400: "#7288b4",
          500: "#355594",
          600: "#304d85",
          700: "#28406f",
          800: "#203359",
          900: "#1a2a49",
        },
      },
    },
    fontFamily: {
      "mine-header": ['"Rubik"', "serif"], // Ensure fonts with spaces have " " surrounding it.
      "mine-poppins": ['"Poppins"', "serif"],
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
