import Particles from "react-tsparticles";
import ParticlesConfig from "./config/particles-config";

export default function ParticlesBackground() {
  const particlesInit = (main) => {
    // console.log(main);
  };

  const particlesLoaded = (container) => {
    // console.log(container);
  };

  return (
    <div>
      <Particles
        id='tsparticles'
        init={particlesInit}
        loaded={particlesLoaded}
        options={{
          ...ParticlesConfig
        }}
      />
    </div>
  );
}
