import React from "react";
import { useState, useEffect } from "react";
import { useDispatch } from "react-redux";
import { signInUser } from "../redux/actions/authenticationAction";

export default function LoginPage() {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  // const result = useSelector(state => state.result)
  const dispatch = useDispatch();

  const handleUsername = (e) => {
    e.preventDefault();
    setUsername(e.target.value);
  };

  const handlePassword = (e) => {
    e.preventDefault();
    setPassword(e.target.value);
  };

  const handleUserSignIn = (e) => {
    e.preventDefault();
    dispatch(
      signInUser({
        username,
        password,
      })
    );
  };

  useEffect(() => {
    console.log(username, password);
  }, [username, password]);

  return (
    <>
      <div className='flex w-full max-w-xs '>
        <form className='bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4 '>
          <div className='mb-4 font-mine-body'>
            <label
              className='block text-gray-700 text-sm font-bold mb-2 font-mine-body'
              htmlFor='username'>
              Username
            </label>
            <input
              className='shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline'
              id='username'
              type='text'
              placeholder='Username'
              onChange={(e) => handleUsername(e)}
            />
          </div>
          <div className='mb-6 font-mine-body'>
            <label
              className='block text-gray-700 text-sm font-bold mb-2 font-mine-body'
              htmlFor='password'>
              Password
            </label>
            <input
              className='shadow appearance-none border border-red-500 rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline'
              id='password'
              type='password'
              placeholder='******************'
              onChange={(e) => handlePassword(e)}
            />
            <p className='text-red-500 text-xs italic font-mine-body'>
              Please choose a password.
            </p>
          </div>
          <div className='flex items-center justify-between'>
            <button
              className='bg-blue-500 hover:bg-blue-700 font-mine-body text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline'
              type='button'
              onClick={(e) => handleUserSignIn(e)}>
              Sign In
            </button>
            <a
              className='inline-block align-baseline font-bold text-sm font-mine-body text-blue-500 hover:text-blue-800'
              href='/'>
              Forgot Password?
            </a>
          </div>
        </form>
      </div>
    </>
  );
}
