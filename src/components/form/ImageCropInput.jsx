import React, { useEffect, useState } from "react";
import Cropper from "react-easy-crop";
// import Slider from '@material-ui/core/Slider'
import getCroppedImg from "../../cropimage";

export default function ImageCropInput({
  imageUrl,
  croppedArea,
  setIsCropping,
  setCroppedImageFor,
}) {
  const aspect = 1;
  
  const [crop, setCrop] = useState({ x: 0, y: 0 });
  const [zoom, setZoom] = useState(1);
  const [croppedAreaPixels, setCroppedAreaPixels] = useState(croppedArea);

  const onCropComplete = (croppedArea, croppedAreaPixels) => {
    setCroppedAreaPixels(croppedAreaPixels);
  };

  const onCrop = async () => {
    const croppedImageUrl = await getCroppedImg(imageUrl, croppedAreaPixels);
    setCroppedImageFor(croppedImageUrl);
  };

  return (
    <>
      <div className=''>
        <div className='absolute top-40 right-20 left-20 bottom-40 bg-gray-100 w-2/4 mx-auto'>
          <Cropper
            image={imageUrl}
            crop={crop}
            zoom={zoom}
            aspect={aspect}
            cropShape='round'
            showGrid={false}
            onCropChange={setCrop}
            onCropComplete={onCropComplete}
            onZoomChange={setZoom}
          />
        </div>
        <div className='flex flex-col bg-gray-100 absolute bottom-0 items-center w-full'>
          <input
            type='range'
            min='1'
            max='3'
            step='0.1'
            value={zoom}
            id='myRange'
            onChange={(e) => setZoom(e.target.value)}
            className='py-6'
          />
          <div className='flex-row'>
            <button
              className='relative rounded h-20 bg-gray-200 text-gray-600 px-8 py-0 mb-4 mx-auto'
              onClick={() => setIsCropping(false)}>
              Cancel
            </button>
            <button
              className='relative rounded h-20 bg-blue-500 text-white px-8 py-0 mb-4 mx-auto'
              onClick={() => {
                setIsCropping(false);
                onCrop();
              }}>
              Save
            </button>
          </div>
        </div>
      </div>
    </>
  );
}
