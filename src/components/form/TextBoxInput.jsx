import React from "react";

export default function TextBoxInput({tag, handleClick, handleChange}) {
  return (
    <>
      <div className='mb-4 font-mine-body'>
        <label
          className='block text-gray-700 text-sm font-bold mb-2 font-mine-body'
          htmlFor='username'>
          {tag}
        </label>
        <input
          className='shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline'
          id='username'
          type='text'
          placeholder={"Enter "+tag}
          onClick={(e) => handleClick(e)}
          onChange={(e) => handleChange(e)}
        />
      </div>
    </>
  );
}
