import React from "react";
import ContentLoader from "react-content-loader";


export default function ContactInfoPlaceholder() {
  return (
    <div className=''>
      <ContentLoader viewBox='0 0 380 70' className='p-4 w-full'>
        <rect x='0' y='0' rx='50' ry='50' width='70' height='70' />
        <rect x='80' y='17' rx='4' ry='4' width='300' height='13' />
        <rect x='80' y='40' rx='3' ry='3' width='250' height='10' />
      </ContentLoader>
      <ContentLoader viewBox='0 0 380 70' className='p-4 w-full'>
        <rect x='0' y='0' rx='50' ry='50' width='70' height='70' />
        <rect x='80' y='17' rx='4' ry='4' width='300' height='13' />
        <rect x='80' y='40' rx='3' ry='3' width='250' height='10' />
      </ContentLoader>
      <ContentLoader viewBox='0 0 380 70' className='p-4 w-full'>
        <rect x='0' y='0' rx='50' ry='50' width='70' height='70' />
        <rect x='80' y='17' rx='4' ry='4' width='300' height='13' />
        <rect x='80' y='40' rx='3' ry='3' width='250' height='10' />
      </ContentLoader>
      <ContentLoader viewBox='0 0 380 70' className='p-4 w-full'>
        <rect x='0' y='0' rx='50' ry='50' width='70' height='70' />
        <rect x='80' y='17' rx='4' ry='4' width='300' height='13' />
        <rect x='80' y='40' rx='3' ry='3' width='250' height='10' />
      </ContentLoader>
    </div>
  );
}
