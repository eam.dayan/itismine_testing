import React from "react";
import ContentLoader from "react-content-loader";

export default function AssetCardPlaceholder() {
  return (
    <>
      <ContentLoader viewBox='0 0 380 200' className='p-4 w-full'>
        <rect x='0' y='0' rx='5' ry='5' width='380' height='200' />
      </ContentLoader>
      <ContentLoader viewBox='0 0 380 70' className='px-4 w-full'>
        <rect x='0' y='0' rx='50' ry='50' width='70' height='70' />
        <rect x='80' y='17' rx='4' ry='4' width='150' height='13' />
        <rect x='80' y='40' rx='3' ry='3' width='90' height='10' />
      </ContentLoader>
      <ContentLoader viewBox='0 0 380 100' className='px-4 pb-16 w-full'>
        <rect x='0' y='17' rx='4' ry='4' width='380' height='13' />
        <rect x='0' y='40' rx='4' ry='4' width='380' height='13' />
        <rect x='0' y='63' rx='4' ry='4' width='380' height='13' />
        <rect x='0' y='86' rx='4' ry='4' width='380' height='13' />
      </ContentLoader>
    </>
  );
}
