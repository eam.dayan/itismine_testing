import React, { useState } from "react";
import { FiSearch } from "react-icons/fi";
import { VscGlobe } from "react-icons/vsc";

export default function Navigation() {
  const [isLoggedIn, setIsLoggedIn] = useState(true);

  return (
    <div className='sticky top-0 bg-white shadow pt-2 z-50'>
      <ul className='flex justify-between'>
        <li className='flex-1 '>
          <a href='/home ' className=''>
            <img
              className='absolute top-2 -left-2.5 z-10 '
              src='../../itismine-transparent.png'
              alt='itismine'
              width={"150"}
            />
            <div className='logo-container'></div>
          </a>
        </li>
        <li className='flex-1 '>
          <a className='nav-button' href='/home'>
            Home
          </a>
          <a className='nav-button' href='/'>
            Feed
          </a>
          <a className='nav-button' href='/'>
            About
          </a>
          <a className='nav-button' href='/'>
            Contact
          </a>
        </li>
        <li className='flex-1 flex-nowrap'>
          <input
            className='textbox w-1/2 py-1 px-3 '
            id='search-bar'
            type='text'
            placeholder='search'
          />
          <label htmlFor='search-bar'>
            <FiSearch className='inline-block text-gold-900 hover:text-gold-500 text-2xl mx-2 ' />
          </label>
          <VscGlobe className='inline-block text-gold-900 hover:text-gold-500 text-2xl mx-2' />
          <a className='nav-button' href='/login'>
            Sign In
          </a>
          <a className='nav-button ' href='/register-profile'>
            Sign Up
          </a>
        </li>
      </ul>
    </div>

    // <div className='sticky top-0 bg-white shadow p-2'>
    //   <nav className='flex items-center sm:flex-row flex-col justify-between flex-wrap bg-teal-500'>
    //     <div className='flex-1'>
    //       <a href='/home ' className='invisible'>
    //         <img
    //           className='absolute top-2 -left-2.5 z-10 '
    //           src='../../itismine-transparent.png'
    //           alt='itismine'
    //           width={"150"}
    //         />
    //         <div className='logo-container'></div>
    //       </a>
    //       <p className=''>login</p>
    //     </div>

    //     <div className='flex-1 sm:flex-row flex-col justify-items-center'>
    //       <a href='/home' className='nav-button'>
    //         Home
    //       </a>
    //       <a href='/' className='nav-button'>
    //         Feed
    //       </a>
    //       <a href='/' className='nav-button'>
    //         About
    //       </a>
    //       <a href='/' className='nav-button'>
    //         Contact
    //       </a>
    //       {isLoggedIn && (
    //         <a href='/' className='nav-button'>
    //           Create_Asset
    //         </a>
    //       )}
    //     </div>

    //     <div className='flex mr-4 flex-col sm:flex-row  items-center'>
    //       <div className='ml-4 items-center flex flex-row justify-items-end'>
    //         <input
    //           type='text'
    //           className='bg-gray-50 hover:bg-gray-100 p-2 pl-3 rounded-lg w-3/4'
    //           placeholder='search'
    //         />
    //         <FiSearch className='text-gold-900 hover:text-gold-500 text-2xl' />
    //         <VscGlobe className='text-gold-900 hover:text-gold-500 text-2xl' />

    //         {isLoggedIn ? (
    //           // TODO: set is logged out to open or close sth
    //           <div>
    //             <div className='ml-4 items-center '>
    //               <a
    //                 href='/login'
    //                 className='flex justify-content-end px-3 py-2 rounded-lg text-white bg-blueberry-500 font-poppins font-bold'>
    //                 Logout
    //               </a>
    //             </div>
    //           </div>
    //         ) : (
    //           <div className='flex flex-row'>
    //             <div className='ml-4 items-center '>
    //               <a
    //                 href='/login'
    //                 className='flex justify-content-end px-3 py-2 rounded-lg text-white bg-blueberry-500 font-poppins font-bold'>
    //                 Sign In
    //               </a>
    //             </div>
    //             <div className='ml-4 items-center '>
    //               <a
    //                 href='/register-profile'
    //                 className='flex justify-content-end px-3 py-2 rounded-lg text-white bg-blueberry-500 font-poppins font-bold'>
    //                 Sign Up
    //               </a>
    //             </div>
    //           </div>
    //         )}
    //       </div>
    //     </div>
    //   </nav>
    // </div>
  );
}
