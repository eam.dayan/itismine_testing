import "./App.css";
import Navigation from "./components/Navigation";
import LoginPage from "./views/LoginPage";
import Home from "./views/Home";
import FeedPage from './views/FeedPage'
import WhitePage from './components/WhitePage'
import Footer from './components/Footer'
import { BrowserRouter, Routes, Route } from "react-router-dom";
import ParticlesBackground from "./ParticlesBackground";
import RegistrationPage from "./views/RegistrationPage";
import ImageCropInput from "./components/form/ImageCropInput"
import ChooseAccountTypePage from "./views/ChooseAccountTypePage";

function App() {
  return (
    <div className='App'>
      <ParticlesBackground />
      <Navigation />
      <BrowserRouter>
        <Routes>
          <Route path='/home' element={<Home />}></Route>
          <Route exact path='/' element={<FeedPage />}></Route>
          <Route path='/test' element={<WhitePage />}></Route>
          <Route path='/login' element={<LoginPage />}></Route>
          <Route path='/register-select' element={<ChooseAccountTypePage />}></Route>
          <Route path='/register-profile' element={<RegistrationPage />}></Route>
          <Route path='/crop' element={<ImageCropInput />}></Route>
        </Routes>
      </BrowserRouter>
      <Footer/>
    </div>
  );
}

export default App;
