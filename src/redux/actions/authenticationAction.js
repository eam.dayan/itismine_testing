import { SIGN_IN, REGISTER_USER } from "../constants/actionTypes";

// export function signInUser(payload) {
//   return { type: SIGN_IN, payload: payload };
// }

// export function registerUser(payload) {
//   return { type: REGISTER_USER, payload: payload };
// }

export const signInUser = (loginInfo) => {
  return async (dispatch) => {
    // const result = await sign_in_user(loginInfo)
    const result = {
      status: 200,
      payload: "successful login",
    };
    dispatch({
      type: SIGN_IN,
      payload: result,
    });
  };
};
