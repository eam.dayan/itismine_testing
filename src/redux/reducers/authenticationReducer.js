import { SIGN_IN, REGISTER_USER } from "../constants/actionTypes";

const initialState = {
  message: "",
  success: false,
};

const authenticationReducer = (state = initialState, action) => {
  //es6 arrow function
  switch (action.type) {
    case SIGN_IN:
      return {
        ...state,
        message: action.payload,
      };
    default: 
      return state;
  }
};

export default authenticationReducer;
