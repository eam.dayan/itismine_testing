
import { createStore, combineReducers } from 'redux';
import authenticationReducer from '../reducers/authenticationReducer'

const rootReducer = combineReducers({authenticationReducer})

const store = createStore(rootReducer);

export default store