import React from "react";
import "../styles/LandingPage.css";
import ContactInfoPlaceholder from "../components/placeholders/ContactInfoPlaceholder";
import AssetCardPlaceholder from "../components/placeholders/AssetCardPlaceholder";

export default function Home() {
  return (
    <>
      <div className='bg-diagonal-transparent-whitesmoke h-v-50 -z-10 px-4 flex items-end'>
        <div className='flex flex-row align-bottom container'>
          <div className=''>
            <img className='object-cover rounded-full h-56 w-56' src='https://image.pngaaa.com/419/263419-middle.png' alt='' />
          </div>
          <div className='flex flex-col ml-16 mt-4'>
            <h1 className='username'>Nou Reach</h1>
            <h1 className='user-role'>Asset Owner</h1>
            <h2 className='user-description'>
              Hello Everyone ! I’m Reach and I am rich . Let me know if you want
              to transfer any assets here. Have a wonderful day.{" "}
            </h2>
          </div>
        </div>
      </div>
      <div className='bg-gray-100 h-full -z-10 p-4'>
        <div className='flex flex-row container mt-16'>
          <div className=' w-1/3'>
            <h1 className='text-left text-2xl py-4 font-semibold'>Contact Information</h1>
            <div className='contact-card'>
              <ContactInfoPlaceholder />
            </div>
          </div>
          <div className=' w-2/3 h-full'>
            <div className='flex-col'>
              <div className='flex flex-row justify-between'>
                <div className='my-4'>
                  <select
                    name='cars'
                    id='cars'
                    className='h-full md:w-80 sm:36 p-2 rounded'>
                    <option value='all'>Asset Type: All</option>
                    <option value='cars'>Asset Type: Cars</option>
                    <option value='motorbikes'>Asset Type: Motorbikes</option>
                    <option value='phones'>Asset Type: Phones</option>
                  </select>
                </div>
                <div className='my-4'>
                  <input
                    className='textbox w-full p-2'
                    id='search-bar'
                    type='text'
                    placeholder='search by token or id'
                  />
                </div>
              </div>
              <h1 className='text-left text-lg mb-4 font-semibold'>
                All assets on the platform:{" "}
                <span className='text-gold-500 font-bold text-lg'>120</span>{" "}
                record
              </h1>
              <div className='flex flex-row flex-wrap'>
                <div className='asset-card'>
                  <AssetCardPlaceholder />
                </div>
                <div className='asset-card'>
                  <AssetCardPlaceholder />
                </div>
                <div className='asset-card'>
                  <AssetCardPlaceholder />
                </div>
                <div className='asset-card'>
                  <AssetCardPlaceholder />
                </div>
                <div className='asset-card'>
                  <AssetCardPlaceholder />
                </div>
                <div className='asset-card'>
                  <AssetCardPlaceholder />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
