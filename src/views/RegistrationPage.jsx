import React, { useState } from "react";
import TextBoxInput from "../components/form/TextBoxInput";
import ImageCropInput from "../components/form/ImageCropInput";
import defaultImage from "../images/user-icon.png";

export default function RegistrationPage() {
  const [firstname, setFirstname] = useState("");
  const [lastname, setLastname] = useState("");
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");

  const [imageUrl, setImageUrl] = useState(defaultImage);
  const [imageFile, setImageFile] = useState("");

  const [croppedArea, setCroppedArea] = useState({
    x: 0,
    y: 0,
    width: 300,
    height: 300,
  });
  const [croppedImage, setCroppedImage] = useState(defaultImage);

  const [isCropping, setIsCropping] = useState(false);

  const onSubmit = (e) => {
    e.preventDefault();
    const profile = {
      firstname,
      lastname,
      email,
      username,
      password,
      confirmPassword,
    };
    console.log(profile);
    console.log(imageFile);
  };

  const ignore = (e) => {};

  const onImageChange = (e) => {
    if (e.target.files && e.target.files[0]) {
      let img = e.target.files[0];
      setImageFile(img);
      setImageUrl(URL.createObjectURL(img));
    }
    setIsCropping(true);
  };

  const setCroppedImageFor = (croppedImageUrl) => {
    setCroppedImage(croppedImageUrl);
  };

  const handleCropPhoto = () => {
    setIsCropping(true);
  };

  console.log(croppedImage);

  return (
    <>
      <div className='flex w-full mx-auto'>
        {isCropping && (
          <div>
            <ImageCropInput
              imageUrl={imageUrl}
              croppedArea={croppedArea}
              setIsCropping={setIsCropping}
              setCroppedArea={setCroppedArea}
              setCroppedImageFor={setCroppedImageFor}
            />
          </div>
        )}

        <form className='bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4 mx-auto'>
          <div className='rounded-full bg-blue-300 mx-auto overflow-auto h-40 w-40 mb-4'>
            <img
              src={croppedImage ? croppedImage : defaultImage}
              alt='cropped'
              onClick={handleCropPhoto}
              className='rounded-full'
            />
          </div>
          <input type='file' name='myImage' onChange={onImageChange} />

          <div>
            <TextBoxInput
              tag='First Name'
              handleChange={(e) => setFirstname(e.target.value)}
              handleClick={(e) => ignore(e)}
            />
            <TextBoxInput
              tag='Last Name'
              handleChange={(e) => setLastname(e.target.value)}
              handleClick={(e) => ignore(e)}
            />
            <TextBoxInput
              tag='Username'
              handleChange={(e) => setUsername(e.target.value)}
              handleClick={(e) => ignore(e)}
            />
            <TextBoxInput
              tag='Email'
              handleChange={(e) => setEmail(e.target.value)}
              handleClick={(e) => ignore(e)}
            />
            <TextBoxInput
              tag='Password'
              handleChange={(e) => setPassword(e.target.value)}
              handleClick={(e) => ignore(e)}
            />
            <TextBoxInput
              tag='Confirm Password'
              handleChange={(e) => setConfirmPassword(e.target.value)}
              handleClick={(e) => ignore(e)}
            />
            <button
              className='bg-blue-500 hover:bg-blue-700 font-mine-body text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline'
              type='button'
              onClick={(e) => onSubmit(e)}>
              Create Account
            </button>
          </div>
        </form>
      </div>
    </>
  );
}
