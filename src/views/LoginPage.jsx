import React from "react";
import LoginCard from '../components/LoginCard'

export default function LoginPage() {
  return (
    <>
      <div className='flex lg:justify-start sm:justify-center items-center h-auto pt-44 lg:pl-40 sm:pl-0 z-0'>
        <LoginCard/>
      </div>
    </>
  );
}
