#!/bin/bash

REDUX_NAME="$1"

mkdir -p src/redux/actions
mkdir -p src/redux/reducers
# mkdir -p src/redux/store

touch src/redux/actions/"$REDUX_NAME"Action.js
touch src/redux/reducers/"$REDUX_NAME"Reducer.js
# touch src/redux/actions/"$REDUX_NAME"Action.js