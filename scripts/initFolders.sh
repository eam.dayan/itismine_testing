#!/bin/bash

touch docker-compose.yaml

mkdir src/components
mkdir src/views
mkdir src/styles
mkdir src/services

mkdir -p src/redux/reducers
mkdir -p src/redux/store
mkdir -p src/redux/actions

