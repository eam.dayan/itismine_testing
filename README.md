# ItIsMine dApp

## How to Change Background Particles

To change the background particle effect

1. Come up with a design in <https://vincentgarreau.com/particles.js> and export as `JSON`

2. Convert the `JSON` to Javascript Object in <https://csvjson.com/json_beautifier>

3. Paste Javascript Object in `./src/config/particles-config.js`
